from django.urls import path
from .views import *

urlpatterns = [

    path('',home, name='home'),
    path('airline_detail/<str:attribute>',airline_detail,name='airline_detail'),
    # path('query/<str:airline_name>',query_local,name='query'),
    path('results/',display_query_result,name='display_query_results'),




]
