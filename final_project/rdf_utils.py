import rdflib
from SPARQLWrapper import SPARQLWrapper

from rdflib import Graph, URIRef
TAG = "[debug] [gema] [views.py]"


def query_local_gema(airline_name):
    graph = Graph()
    result = graph.parse('local.ttl', format='n3')
    print(TAG, "Graph: {} statements".format(len(graph)))

    query = """PREFIX ex: <http://example.org/>
        PREFIX exp: <http://example.org/property/>
        PREFIX dbo: <http://dbpedia.org/ontology/>
        PREFIX dbp: <http://dbpedia.org/property/>
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        SELECT ?name ?country ?isActive ?callsign ?iataAirlineCode ?icaoAirlineCode
        WHERE {
            ?airline foaf:name ?name
            FILTER (regex(?name, "%s", "i")) .
            ?airline dbo:country ?country .
            ?airline exp:isActive ?isActive .
            ?airline dbo:iataAirlineCode ?iataAirlineCode .
            ?airline dbo:icaoAirlineCode ?icaoAirlineCode .
            ?airline dbp:callsign ?callsign .
        } """ % airline_name

    query_result = graph.query(query)
    print('query result:\n', query_result)

    final_result = []

    for result in query_result:
        result = result.asdict()
        row = []
        for key in result:
            row.append(result[key].toPython())

        # (name, country, isActive, callsign, iataAirlineCode, icaoAirlineCode)
        row = tuple(row)
        final_result.append(row)
        # print(TAG, row)
    final_result = list(set(final_result))
    print('final result:\n\n')
    final_final_result = []
    for item in final_result:
        value = list(item)
        key = ["name", "country", "isActive", "callsign", "iata", "icao", "url"]
        value.append(("&".join(item)).replace(" ", "%20"))
        final_final_result.append(dict(zip(key, value)))
    return final_final_result

def local_query(airline_name):
    print("Querying local data")
    g = rdflib.Graph()
    result = g.parse("local.ttl", format="n3")

    print("graph has %s triples" % len(result))

    query = """PREFIX exo: <http://example.org/ontology/>
            PREFIX exp: <http://example.org/property/>
            PREFIX dbo: <http://dbpedia.org/ontology/>
            PREFIX dbp: <http://dbpedia.org/property/>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            SELECT ?AirlineName ?IATA ?ICAO ?Callsign ?Country ?IsActive
            WHERE {
                ?Airline foaf:name ?AirlineName .
                FILTER REGEX (str(?AirlineName), "%s") .
                OPTIONAL { ?Airline dbo:iataAirlineCode ?IATA . }
                OPTIONAL { ?Airline dbo:icaoAirlineCode ?ICAO . }
                OPTIONAL { ?Airline dbp:callsign ?Callsign . }
                OPTIONAL { ?Airline dbo:country ?Country . }
                ?Airline exp:isActive ?IsActive .
            } """ % airline_name
    
    results = g.query(query)
    print("length of result is %s" % len(results))
    # for row in query_result:
    #     print(row)
    return results


def dbpedia_query(airline_name):
    print("Querying remote data on dbpedia.org")
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    query = """
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        PREFIX dbp: <http://dbpedia.org/property/>
        PREFIX dbr: <http://dbpedia.org/resource/>
        PREFIX dbo: <http://dbpedia.org/ontology/>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        SELECT ?abstract ?AirlineName ?Headquarter ?Slogan ?FleetSize ?comment ?hubAirport
        WHERE { 
            ?Airline foaf:name ?AirlineName .
            FILTER REGEX (str(?AirlineName), "%s") .
            OPTIONAL { 
            ?Airline dbo:abstract ?abstract . 
            FILTER (lang(?abstract) = 'en')
            
            }
            OPTIONAL { 
            ?Airline rdfs:comment ?comment .
             FILTER (lang(?comment) = 'en') 
             }
            OPTIONAL { ?Airline dbo:headquarter ?Headquarter . }
            OPTIONAL { ?Airline dbp:companySlogan ?Slogan .}
            
            OPTIONAL { ?Airline dbo:hubAirport ?hubAirport . }
            OPTIONAL { ?Airline dbp:fleetSize ?FleetSize . }
            
            
            
            

         
        }""" % airline_name
    
    sparql.setQuery(query)
    sparql.setReturnFormat("json")
    results = sparql.query().convert()


    return results["results"]["bindings"]


if __name__ == '__main__':
    dbpedia_query("Garuda Indonesia")
